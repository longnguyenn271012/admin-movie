import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Layout from "../../HOCs/layout";
import { fetchMe } from "../../store/actions/auth";

const Dashboard = (props) => {
  const me = useSelector((state) => state.auth.auth);

  useEffect(() => {
    if (!me) props.history.push("/admin/login");
  });

  return (
    <Layout>
      <div className="users__page container-fluid ">
        <div className="header row">
          <div className="header__tittle col-3 py-3">
            <span className="fs-2">Dashboard</span>
            <span className="fs-6 ms-5 text-white-50">3,702 Total</span>
          </div>
        </div>
        <div className="row">
          <div className="col-6 bg bg-secondary">
            <span className="d-block">Total Movies</span>
            <span>254</span>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Dashboard;
